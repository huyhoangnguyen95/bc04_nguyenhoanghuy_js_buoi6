// Bài 1

function xemKetQua() {
    // console.log("yes");
    var sum = 0;

    for (var i = 1; sum < 10000; i++) {
        sum += i;
        // console.log('sum: ', sum , i);
        document.getElementById("resultBai1").innerHTML = ` ${i} `;
    }
}



// Bài 2 

function tinhTong() {
    // console.log("yes");
    var numX = document.getElementById("txt-numberX").value * 1;
    var numN = document.getElementById("txt-numberN").value * 1;
    var result = 0;

    for (var i = 1; i <= numN; i++) {
        result += numX ** i;
        // console.log('result: ', result);
    }
    document.getElementById("resultBai2").innerHTML = ` Tổng : ${result} `
}


// Bài 3

function tinhGiaiThua() {
    // console.log("yes");
    var numN = document.getElementById('txt-numN').value * 1;
    var result = 1;
    for (var i = 1; i <= numN; i++) {
        result *= i;
        // console.log('result: ', result);
    }
    document.getElementById("resultBai3").innerHTML = ` Giai thừa : ${result} `

}


// Bài 4

function taoThe() {
    // console.log("yes");
    var contentHTML = "";
    for (var i = 1; i <= 10; i++) {
        if (i % 2 == 0) {
            contentHTML += ` <div class="alert alert-danger">Đây là số chẵn</div> `;

        } else {
            contentHTML += ` <div class="alert alert-primary">Đây là số lẻ</div> `;

        }
    }
    document.getElementById("resultBai4").innerHTML = ` ${contentHTML} `;


}